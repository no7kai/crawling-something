from selenium import webdriver
from selenium.webdriver.firefox.options import Options # use headless mode with geckodriver by using the headless option
import time
import csv


# Create log in function
def fb_login():
    driver.get('https://www.facebook.com')
    username = driver.find_element_by_id('email')
    password = driver.find_element_by_id('pass')
    username.send_keys(user)
    password.send_keys(pws)
    driver.find_element_by_id('loginbutton').click()
    time.sleep(2)
    
today = time.strftime('%Y-%m_%d')

csv_file = open('user_gender.csv', 'w')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['uid', 'gender'])

logger = logging.getLogger('FB_ads')
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
f_handler = logging.FileHandler(f'{today}_ads_log.log')
f_handler.setLevel(logging.INFO)
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
f_handler.setFormatter(f_format)
c_handler.setFormatter(f_format)
logger.addHandler(f_handler)
logger.addHandler(c_handler)

# Config FF driver
options = Options()
options.headless = True
driver = webdriver.Firefox(executable_path='/media/no7kai/JAV/Venesa/FB_crawling/scraping_js_rendered/geckodriver-v0.24.0-linux64/geckodriver')

# Get list of accounts
user_ls = [] 
with open('user_list.txt') as f:
    for line in f:
        line = line.strip('\n')
        user_ls.append(line.split())
# Pick a account
user, pws = user_ls.pop()

# Login to facebook
fb_login()
# Check if account logs in successfully 
while driver.current_url != 'https://www.facebook.com/':
    logger.warning(f'{user} failed to login, try another account!')
    try:
        user, pws = user_ls.pop()
        fb_login()
    except IndexError:
        raise Exception('There is no more account to log in.')

start = time.time()
count = 0
# Get list of uids
with open('kol.csv') as f:
    for line in f:
        count += 1
        uid = line.strip('\n')
        url = f'https://www.facebook.com/{uid}/about'
        driver.get(url)
        try:            
            # Get text which includes user's gender
            local = driver.find_element_by_xpath("//*[contains(@data-testid, 'nav_places')]").text
        except Exception as e:
            print(e)
            try:
                # Check if account has been blocked
                blocked = driver.find_element_by_xpath("//*[contains(@class, 'uiHeader uiHeaderBottomBorder mhl mts')]")
                print(blocked.text)
                break
            except Exception as e:
                print(e)
            continue
        print(count)
        print(uid, local)
        time.sleep(1)
finish = time.time()
duation = finish - start
