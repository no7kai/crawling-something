import pandas as pd
import csv

df = pd.read_csv('hn_group_like.csv', chunksize=50000)

beauty_category = ['Beauty Store',
                   'Beauty Supply Store',
                   'Health/Beauty',
                   'Beauty Salon',
                   'Tanning Salon',
                   'Beauty Supplier',
                   'Makeup Artist',
                   'Skin Care Service',
                   'Spa',
                   'Aromatherapy Service',
                   'Day Spa',
                   'Health Spa',
                   'Massage Service',
                   'Beauty, Cosmetic & Personal Care']


beauty = open('hn_beauty_pages.csv', 'a')
beauty_writer = csv.writer(beauty)
non_beauty = open('hn_not_beauty_pages.csv', 'a')
non_beauty_writer = csv.writer(non_beauty)

for data in df:
    for index, row in data.iterrows():
        pages = row['likes']
        beauty_pages = []
        try:
            pages = pages.strip(';')
            pages = pages.split(';')
            for page in pages:
                cat = page.split('-')
                if cat[-1] in beauty_category:
                    beauty_pages.append(page)
            if beauty_pages:
                pages = '; '.join(beauty_pages)
                beauty_writer.writerow([row['_id'], pages])
            else:
                non_beauty_writer.writerow([row['_id'], row['likes']])
        except Exception:
            non_beauty_writer.writerow([row['_id'], row['likes']])
            continue
beauty.close()
non_beauty.close()
