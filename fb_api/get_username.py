import pandas as pd
import requests

df = pd.read_csv('kol.csv', header=None)
token = 'EAAAAUaZA8jlABAIgGxge9yT6Xf1fu3mflwzzvcbbpyQpVu0AniZA5yHLnzRkZALqZCCDeSostWu2Me9V9kJZARFEqEPmfAjf8rZCLPXEUIYCZBCfrmU2zQ07W7xYMO13KQS2EiBu6EBPRvuy0ZAVsJEZA3zoKfi0MZAfdfdkPml1UNoroFgc9ZC3AKA9fGX01Kd2TkZD'
name_list = []
for uid in df[0]:
    url = f'https://graph.facebook.com/{uid}/?access_token={token}'
    res = requests.get(url)
    if res.status_code == 200:
        r = res.json()
        name_list.append(r['name'])
    else:
        name_list.append('Not exist')

name_col = pd.Series(name_list, name='Name')
df = df.join(name_col)
df.to_csv('kol_name.csv')
