import pandas as pd

df = pd.read_csv('no_location_gender.csv')

patterns = {'[àáảãạăắằẵặẳâầấậẫẩ]': 'a',
            '[đ]': 'd',
            '[èéẻẽẹêềếểễệ]': 'e',
            '[ìíỉĩị]': 'i',
            '[òóỏõọôồốổỗộơờớởỡợ]': 'o',
            '[ùúủũụưừứửữự]': 'u',
            '[ỳýỷỹỵ]': 'y'
            }


def conv(text):
    if type(text) is not str:
        return text
    text = text.lower()
    text = ''.join(text.split(','))
    text = ''.join(text.split(' '))
    for char in text:
        for key, value in patterns.items():
            if char in key:
                text = text.replace(char, value)

    if 'hanoi' in text:
        text = 'ha_noi'
    elif 'haiphong' in text:
        text = 'hai_phong'
    elif 'cantho' in text:
        text = 'can_tho'
    elif 'hochiminh' in text:
        text = 'ho_chi_minh'
    return text


df['hometown'] = df['hometown'].apply(conv)

df['location2'] = df['location2'].apply(conv)

li = []
loc = ['ha_noi', 'hai_phong', 'ho_chi_minh', 'can_tho']
for index, row in df.iterrows():
    if row['hometown'] in loc or row['location2'] in loc:
        li.append((row['_id'], row['hometown'], row['location2']))


df = pd.DataFrame(li, columns=['uid', 'hometown', 'location'])

df.to_csv('filtered_location_06-05-2019.csv', index=False)
