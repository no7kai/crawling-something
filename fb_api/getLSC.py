import requests
import pandas as pd
import time
import logging


s = requests.Session()
adapter = requests.adapters.HTTPAdapter(max_retries=50)
s.mount('https://', adapter)

tokens = []
with open('tokens.csv') as f:
    for line in f:
        line = line.strip('\n')
        tokens.append(line)
token = tokens.pop(0)

today = time.strftime('%Y-%m-%d')

logger = logging.getLogger('FB_posts')
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler(f'{today}_ads(lsm).log', mode='w')
f_handler.setLevel(logging.INFO)
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
f_handler.setFormatter(f_format)
logger.addHandler(f_handler)

df = pd.read_csv(f'{today}_ads.csv')

like_series = []
share_series = []
cmt_series = []
content_series = []
time_series = []

for ads_id in df['adsId']:
    print(ads_id)
    url = f'https://graph.facebook.com/v2.1/{ads_id}/?access_token={token}'
    res = s.get(url)
    r = res.json()
    print(res.status_code)
    while res.status_code != 200:
        print(res.status_code)
        if r['error']['code'] == 190:
            print('yes')
            logger.waring(f"{token} - This token is invalidated")
            try:
                token = tokens.pop(0)
            except IndexError:
                logger.exception("Out of token!")
                raise Exception("Out of token!")
            url = f'https://graph.facebook.com/v2.1/{ads_id}/?access_token={token}'
            res = s.get(url)
            r = res.json()
        break
    try:
        time = r['created_time']
    except KeyError:
        logger.exception(f'Cannot find created time - {ads_id}')
        time = None
    time_series.append(time)
    try:
        like = r['likes']['count']
    except KeyError:
        logger.exception(f'Cannot find like - {ads_id}')
        like = None
    like_series.append(like)
    try:
        share = r['shares']['count']
    except KeyError:
        logger.exception(f'Cannot find share - {ads_id}')
        share = None
    share_series.append(share)
    try:
        comment = r['comments']['count']
    except KeyError:
        logger.exception(f'Cannot find comment - {ads_id}')
        comment = None
    cmt_series.append(comment)
    try:
        content = r['message']
    except KeyError:
        logger.exception(f'Cannot find message - {ads_id}')
        content = None
    content_series.append(content)

content_col = pd.Series(content_series, name='content')
like_col = pd.Series(like_series, name='likes')
share_col = pd.Series(share_series, name='shares')
cmt_col = pd.Series(cmt_series, name='comments')
time_col = pd.Series(time_series, name='createdTime')
df = df.join(content_col)
df = df.join(like_col)
df = df.join(share_col)
df = df.join(cmt_col)
df = df.join(time_col)

df.to_csv(f'Ads from 50 pages {today}.csv')
logger.info(f'Finish crawling FB posts {today}')
