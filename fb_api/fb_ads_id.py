__doc__ = '''
Crawl facebook ads bằng selenium, giờ đã không còn dùng được
'''


from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import NoSuchElementException
import time
import csv
import logging


def fb_login():
    driver.get('https://www.facebook.com')
    username = driver.find_element_by_id('email')
    password = driver.find_element_by_id('pass')
    username.send_keys('tiffany.msutton@yahoo.com')
    password.send_keys('buithai2019')
    driver.find_element_by_id('loginbutton').click()
    time.sleep(2)

today = time.strftime('%Y-%m_%d')

csv_file = open(f'{today}_ads2.csv', 'w')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['Page', 'adsId'])

logger = logging.getLogger('FB_ads')
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
f_handler = logging.FileHandler(f'{today}_ads_log.log')
f_handler.setLevel(logging.INFO)
f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
f_handler.setFormatter(f_format)
c_handler.setFormatter(f_format)
logger.addHandler(f_handler)
logger.addHandler(c_handler)

options = Options()
options.headless = True
driver = webdriver.Firefox(executable_path='/media/no7kai/JAV/Venesa/FB_crawling/scraping_js_rendered/geckodriver-v0.24.0-linux64/geckodriver')

# # Get list of accounts
# user_ls = []
# with open('user_list.txt') as f:
#     for line in f:
#         line = line.strip('\n')
#         user_ls.append(line.split())
# # Pick a account
# user, pws = user_ls.pop()

# Open and login to Facebook
fb_login()
# Check if account logs in successfully
while driver.current_url != 'https://www.facebook.com/':
    logger.warning(f'{user} failed to login, try another account!')
    try:
        user, pws = user_ls.pop()
        fb_login()
    except IndexError:
        raise Exception('There is no more account to log in.')
logger.info('Logging in successful.')
# Open list 50 Beauty pages
with open('50pages_wid (copy).csv') as f:
    for line in f:
        line = line.strip('\n')
        line = line.split(',')
        page = line[1]
        url = line[2]
        uid = line[3]

        # Open url again, click to "Ads" tab then load page
        driver.get(url)
        time.sleep(5)
        # Get rid of fucking blocks again
        try:
            driver.find_element_by_xpath("//*[contains(@class, 'autofocus layerCancel')]").click()
        except Exception as e:
            logger.error(e)
            pass
        try:
            driver.find_element_by_xpath("//*[@class='_3ixn']").click()
        except Exception as e:
            logger.error(e)
            pass
        # Click to Ads tab
        driver.find_element_by_xpath("//*[contains(@data-key, 'tab_ads')]").click()
        time.sleep(5)
        # Get rid of fucking blocks again
        try:
            driver.find_element_by_xpath("//*[contains(@class, 'autofocus layerCancel')]").click()
        except Exception:
            pass
        # Scroll 5 times to get more ads
        for i in range(5):
            # execute script to scroll down the page
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
            # sleep for 9s
            time.sleep(8)

        results = driver.find_elements_by_xpath("//*[contains(@class,'_1dwg _1w_m _q7o')]")

        # Log result of getting ads
        if len(results) == 0:
            logger.info(f"Page {page} hasn't run any ads")
            continue
        else:
            logger.info(f"Got {len(results)} ads from Page {page}")

        # Find id and content of ads
        for result in results:
            ads = result.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']")
            ads_id = ads.find_element_by_tag_name('div').get_attribute('id')
            ads_id = ads_id.split('_')[2]
            if ':' in ads_id:
                ads_id = ads_id.split(':')[0]
                adsId = f'{uid}_{ads_id}'
            elif ';' in ads_id:
                ads_id = ads_id.split(';')[:2]
                adsId = '_'.join(ads_id)
            csv_writer.writerow([page, adsId])

driver.quit()
csv_file.close()
logger.info('Finish crawling Fb ads form 50 pages!')
