from pymongo import MongoClient
import csv
import pandas as pd

client = MongoClient()
mydb = client.bigdata_2019
col = mydb.vnphonefullinfos

csv_file = open('hn_user.csv', 'w')
csv_writer = csv.writer(csv_file)

df = pd.read_csv('hn_beauty_pages.csv', chunksize=10000, names=['uid', 'pages'])
for data in df:
    for uid in data['uid']:
        uid = str(uid)
        user = col.find_one({'_id': uid})
        csv_writer.writerow([user['name'], user['phone']])

csv_file.close()
