import pandas as pd
import csv


df = pd.read_csv('cantho_group_like.csv', chunksize=100000)

dic = [('0162', '032'),
        ('0163', '033'),
        ('0164', '034'),
        ('0165', '035'),
        ('0166', '036'),
        ('0167', '037'),
        ('0168', '038'),
        ('0169', '039'),
        ('0120', '070'),
        ('0121', '079'),
        ('0122', '077'),
        ('0126', '076'),
        ('0128', '078'),
        ('0123', '083'),
        ('0124', '084'),
        ('0125', '085'),
        ('0127', '081'),
        ('0129', '082'),
        ('0186', '056'),
        ('0188', '058'),
        ('0199', '059')]


def convertor(n):
    n = str(n)
    if n.startswith('84'):
        n = n.replace('84', '0', 1)
    for old, new in dic:
        if old in n:
            return n.replace(old, new, 1)
    return n


patterns = {'àáảãạăắằẵặẳâầấậẫẩ': 'a',
            'đ': 'd',
            'èéẻẽẹêềếểễệ': 'e',
            'ìíỉĩị': 'i',
            'òóỏõọôồốổỗộơờớởỡợ': 'o',
            'ùúủũụưừứửữự': 'u',
            'ỳýỷỹỵ': 'y',
            '!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~': ''
            }


def conv(text):
    if type(text) is not str:
        return text
    text = text.lower()
    text = ''.join(text.split(','))
    text = ''.join(text.split(' '))
    for char in text:
        for key, value in patterns.items():
            if char in key:
                text = text.replace(char, value)
    return text


beauty_category = ['Beauty Store',
                   'Beauty Supply Store',
                   'Health/Beauty',
                   'Beauty Salon',
                   'Tanning Salon',
                   'Beauty Supplier',
                   'Makeup Artist',
                   'Skin Care Service',
                   'Spa',
                   'Aromatherapy Service',
                   'Day Spa',
                   'Health Spa',
                   'Massage Service',
                   'Beauty, Cosmetic & Personal Care']

beauty_keywords = ['beautystore',
                   'beautysupplystore',
                   'health',
                   'beautysalon',
                   'tanningsalon',
                   'beautysupplier',
                   'makeupartist',
                   'skincareservice',
                   'spa',
                   'aromatherapyservice',
                   'dayspa',
                   'healthspa',
                   'massageservice',
                   'beautycosmetic&personalcare',
                   'thammy',
                   'lamdep',
                   'mypham',
                   'trangdiem',
                   'chamsocda',
                   'beauty',
                   'damat',
                   'giambeo',
                   'triseo',
                   'trehoa',
                   'nangmui',
                   'tannhang',
                   'trimun',
                   'trietlong',
                   'trinam'
                   ]

beauty = open('cantho_beauty_pages.csv', 'w')
beauty_writer = csv.writer(beauty)
beauty_writer.writerow(['uid', 'name', 'number', 'converted_number', 'pages', 'groups'])
non_beauty = open('cantho_not_beauty_pages.csv', 'w')
non_beauty_writer = csv.writer(non_beauty)
non_beauty_writer.writerow(['uid', 'name', 'number', 'converted_number', 'pages', 'groups'])

for data in df:
    data.fillna('', inplace=True)
    for index, row in data.iterrows():
        num = convertor(row['phone'])
        pages = row['likes']
        beauty_pages = []
        groups = row['groups']
        beauty_groups = []
        pages = pages.strip(';').split(';')
        groups = groups.strip(';').split(';')
        for page in pages:
            cat = page.split('-')
            if cat[-1] in beauty_category:
                beauty_pages.append(page)
        for group in groups:
            for kw in beauty_keywords:
                if kw in conv(group):
                    beauty_groups.append(group)
                    break
        if beauty_pages or beauty_groups:
            pages = '; '.join(beauty_pages)
            groups = '; '.join(beauty_groups)
            beauty_writer.writerow([row['_id'], row['name'], row['phone'], num, pages, groups]) 
        else:
            non_beauty_writer.writerow([row['_id'], row['name'], row['phone'], num, pages, groups])
beauty.close()
non_beauty.close()
