import os
from PyPDF2 import PdfFileWriter, PdfFileReader
import concurrent.futures


def get_invoices(path):
    invoices = []
    for root, _, files in os.walk(path):
        for f in files:
            if 'Transaction' in f:
                f = os.path.join(root, f)
                invoices.append(f)
    return invoices


def create_watermark(input_pdf):
    watermark_obj = PdfFileReader('watermark.pdf')
    watermark_page = watermark_obj.getPage(0)

    pdf_reader = PdfFileReader(input_pdf)
    pdf_writer = PdfFileWriter()

    for i in range(pdf_reader.getNumPages()):
        page = pdf_reader.getPage(i)
        if i == 0:
            page.mergePage(watermark_page)
        pdf_writer.addPage(page)

    with open(input_pdf, 'wb') as out:
        pdf_writer.write(out)


if __name__ == '__main__':
    path = os.getcwd()
    invoices = get_invoices(path)
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(create_watermark, invoices)
