import csv
import sys
import pygsheets
import pandas as pd
import threading
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.user import User

sys.path.append('/home/no7kai/.local/lib/python3.6/site-packages')  # Replace this with the place you installed facebookads using pip
sys.path.append('/home/no7kai/.local/lib/python3.6/site-packages/facebook_business-3.2.12-py3.6.egg-info')  # same as above

my_app_id = '748522478915763'
my_app_secret = '3a72521707fce131e78a623d6c6cca8f'
my_access_token = 'EAAKoxvh2ULMBAKLf2zcuEcPQQTZBN0rKXTvd2ZCIuPFG9rbiM85gYmA0vuPMgcalJ3eSWZCXXqZAtZA7FoU0qcU5PT8juHl4T8LSgRMrCDkTbwC9ZBiBrVH6PqAkfPBKZCYMjNrjJs06ZBtMY2EyO2CJZBrN82UK6hx2QVahZBlQ3GkAZDZD'
FacebookAdsApi.init(my_app_id, my_app_secret, my_access_token)

user = User('2479474052115295')
ad_accounts = user.get_ad_accounts()


def lead_insights(interval):
    insight_fields = [
        'actions',
        'ad_name',
        'ad_id',
        'adset_name',
        'campaign_name',
        'clicks',
        'frequency',
        'reach',
        'spend',
        'impressions',
    ]
    id_fields = ['effective_object_story_id', 'body']
    insight_params = {
            'action_breakdowns': ['action_type'],
            'date_preset': interval
    }
    # Open a csv file
    header = [
            'Link to promoted post',
            'Campaign name',
            'Adset name',
            'Ad name',
            'Ad id',
            'Status',
            'Impressions',
            'Reach',
            'Clicks (all)',
            'Leads',
            'Frequency',
            'Cost'
    ]
    f = open('lead_{}.csv'.format(interval), 'w')
    writer = csv.writer(f)
    writer.writerow(header)
    # Loop through ad_counts finding data
    for acc in ad_accounts:
        campaigns = acc.get_campaigns(fields=['status'], params={'date_preset': interval})
        for cam in campaigns:
            # Only get ads from active campaigns
            if cam['status'] == 'ACTIVE':
                ads = cam.get_ads(fields=['status'], params={'date_preset': interval})
                for ad in ads:
                    ad_status = ad['status']
                    insights = ad.get_insights(fields=insight_fields, params=insight_params)
                    ad_creative = ad.get_ad_creatives(fields=id_fields)[0]
                    url = 'https://www.facebook.com/{}'.format(ad_creative['effective_object_story_id'])
                    try:
                        body = ad_creative['body'].splitlines()
                        body = '\n'.join(body[:2])
                        content = '=HYPERLINK("{}", "{}")'.format(url, body)
                    except KeyError:
                        content = url
                    print(ad_status, url)
                    if insights:
                        try:
                            leads = 0
                            for action in insights[0]['actions']:
                                print(action['action_type'])
                                if action['action_type'] in ['onsite_conversion.messaging_conversation_started_7d', 'leadgen.other']:
                                    leads = action['value']
                        except KeyError:
                            leads = 0
                        clicks = insights[0]['clicks']
                        frequency = insights[0]['frequency']
                        reach = insights[0]['reach']
                        cost = insights[0]['spend']
                        impressions = insights[0]['impressions']
                        ad_name = insights[0]['ad_name']
                        ad_id = ad['id']
                        adset_name = insights[0]['adset_name']
                        campaign_name = insights[0]['campaign_name']
                        row = [
                                content,
                                campaign_name,
                                adset_name,
                                ad_name,
                                ad_id,
                                ad_status,
                                impressions,
                                reach,
                                clicks,
                                leads,
                                frequency,
                                cost
                        ]
                        writer.writerow(row)
                    else:
                        print('Cannot get data from this ad {}'.format(ad))
    f.close()
    return None


def csv2ggsheets(interval):
    # Open csv file by pandas
    df = pd.read_csv('lead_{}.csv'.format(interval))
    df['Ad id'] = df['Ad id'].astype(str)

    # Open the gg spreadsheet
    gc = pygsheets.authorize(service_account_file='secret.json')
    sheet = gc.open('Lead Tracking')
    worksheet = sheet.worksheet_by_title(interval)
    worksheet.clear()
    worksheet.set_dataframe(df, (1,1))

    return None


def unique_link():
    df = pd.read_csv('lead_yesterday.csv')
    if not df.empty:
        df = df.groupby([
            'Link to promoted post',
            'Campaign name',
            'Adset name'])['Impressions', 'Reach', 'Clicks (all)', 'Leads', 'Cost'].sum()
        df.reset_index(inplace=True)

    # Open the gg spreadsheet
    gc = pygsheets.authorize(service_account_file='secret.json')
    sheet = gc.open('Lead Tracking')
    worksheet = sheet.worksheet_by_title('yesterday_unique_link')
    worksheet.clear()
    worksheet.set_dataframe(df, (1, 1))


def main(interval):
    lead_insights(interval)
    csv2ggsheets(interval)
    if interval == 'yesterday':
        unique_link()

def sum_(*args):
    print(args)
    return sum(args)


if __name__ == '__main__':
    thread1 = threading.Thread(target=main, args=('yesterday',))
    thread2 = threading.Thread(target=main, args=('last_7d',))
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
#     csv2ggsheets('last_7d')
#     csv2ggsheets('yesterday')
#     main('yesterday')
#     main('last_7d')

